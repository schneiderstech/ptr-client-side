# Restaurant App for your friends!!
> This is an App to share with your feature teams to decide by vote where to go for lunch.

## Requirements:

To run and set our app we need to have some packages:

- NPM 
    - Package to install our app packages. For more, info visit: `https://www.npmjs.com`.
- Node.js 
    - For the server side app. More info at: `https://nodejs.org/en/`.
- Express 
    - Which will be our web application framework for Node.js, more info at: `https://expressjs.com`.
- MongoDB 
    - To store our records. For more, info visit: `https://www.mongodb.com`.


## Installing our App

#### 1 Step: 
Paste `restaurant-app` folder in your directory.

#### 2 Step:
In order to install all packages required for the `server-side` and `client-side` of our app, we need to go in the main directory of the folder `ptr-server-side` and folder `ptr-client-side` to run the following command:
```
$ npm install 
```


## Usage

To start the app, just run the following command in the server side folder `ptr-server-side`:

```
$ npm start
```


## Tests
We have implemented Server Side and Client Side test using `Mocha` and `Protractor`, respectively. To run them, we follow the step described below:

### - Server Side
In the main folder `ptr-server-side`, follow the steps:
Run the command:
```
$ npm test
```

### - Client Side
In the main folder `ptr-client-side`, follow the steps:

##### 1 Step:
Start the WebDriver Manager by running: 
```
$ webdriver-manager start
```
##### 2 Step:
Run the tests.config: 
- To run all suites:
```
$ protractor src/tests.config.js --suites
```
- To run a single suite:
```
$ protractor src/tests.config.js --suite suite1
```


## Recommendations
Integrate Webpack to bundle our modules.

exports.config = {  
    /* 
        First Step:
        # Start the WebDriver Manager by running: 
            webdriver-manager start
        Second Step:
        # Run the tests.config: 
            - To run all suites:
            protractor src/tests.config.js --suites

            - To run a single suite:
            protractor src/tests.config.js --suite <desired suite>
    */

    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',  
    getPageTimeout: 60000,  
    allScriptsTimeout: 500000,            
    suites: {
        suite1: '../src/components/main/main.spec.js',
        suite2: '../src/components/header/header.spec.js',
        suite3: '../src/components/menu/menu.spec.js',
        suite4: '../src/components/user/user.spec.js',  
        suite5: '../src/components/restaurant/restaurant.spec.js'  
    }
};

    // Declare app level module which depends on views, and components
    const adminApp = angular.module('adminApp', [
        'ui.router',
        'ngSanitize',
        'ngAnimate',
        'ngMaterial',
        'ngCookies',
        'angularMoment',
        'angular.filter'
    ]);

    adminApp.config(function($locationProvider, $urlRouterProvider, $stateProvider){    
        $urlRouterProvider.otherwise('/');
        // to remove #! from url
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });    
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl : 'views/appView.html',
                controller: 'AppController'
            }) 
            .state('header', {
                url: '/',
                templateUrl : 'headerView.html',
                controller: 'headerController'
            })                   
            .state('main', {
                url: '/',
                templateUrl : 'mainView.html',
                controller: 'MainController'
            })
            .state('restaurant', {
                url: '/',
                templateUrl : 'components/restaurant/restaurantView.html',
                controller: 'RestaurantController'
            })
            .state('user', {
                url: '/',
                templateUrl : 'components/user/userView.html',
                controller: 'UserController'
            })
            .state('menu', {
                url: '/',
                templateUrl : 'menuView.html',
                controller: 'MenuController'
            });    

    });

    adminApp.controller('AppController', ['$scope', function($scope) {

        $scope.pageSettings = {
            scroolTop: {
                img: 'ic_arrow_up_24px',
                alt: 'Back to top'
            }
        }

        $scope.scrollToTop = function() {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('.back-to-top').fadeIn();
                } else {
                    $('.back-to-top').fadeOut();
                }
            });

            $('.back-to-top').click(function () {
                $("html, body").animate({ scrollTop: 0 }, 500);
                return false;
            });
        };

    }]);

    adminApp.directive('directiveBackToTop', function() {
        return {
            templateUrl: '/directives/back-to-top.html'
        };
    });    

(function() { 
    'use strict';

    adminApp.controller('HeaderController', ['$scope',function($scope) {
        let standardPath = '/assets/img/icons/';
        
        $scope.pageSettings = {
            title: 'Restaurant App',
            home: {
                path: standardPath,
                name: 'ic_home_24px.svg',
                alt: 'Home'
            }
        }
    }]);
})();
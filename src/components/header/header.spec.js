describe('Angular Material Header Section', function() {
    let homeButton = element(by.name('homeButton'));
    
    it('should browser in the main page', function(){
        browser.get('http://localhost:8080');
    });

    it('should have a Home Button', function() {
        expect(homeButton.isPresent()).toBe(true);
    });    
});
(function() { 
    'use strict';

    adminApp.controller('MenuController', ['$scope', '$state', function($scope, $state) {

        let originatorEv;
        let standardPath = '/assets/img/icons/';

        this.openMenu = function($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };
    
        $scope.pageSettings = {
            title: 'Menu',
            menuContentWidth: 4,
            menuItems: [
                {
                    title: 'User',
                    buttonAction: {
                        key:'userPage',
                        value: 1,
                        link: 'user'
                    },
                    icon: {
                        path: standardPath,
                        name: 'ic_person_24px.svg',
                    }
                },
                {
                    title: 'Restaurant',
                    buttonAction: {
                        key: 'restaurantPage',
                        value: 2,
                        link: 'restaurant'
                    },
                    icon: {
                        path: standardPath,
                        name: 'ic_place_24px.svg'
                    }
                }
            ]
        };

        $scope.buttonAction = function(buttonAction) {
            let linkRouteProvider = buttonAction.link;
            $state.go(linkRouteProvider);    
        };

    }]);
})();
describe('Angular Material Restaurant Section', function() {
    let menuClick = element(by.id('open-menu'));
    let restaurantClick = element(by.id('restaurantPage'));
    let pageTitle = element(by.name('page-title'));
    let restaurantDescriptionField = element(by.name('description'));
    let buttonSave = element(by.name('submitForm'));
    
    it('should click in the menu icon', function(){
        browser.get('http://localhost:8080')
        .then(function(){
            menuClick.click();
        });
    });
    it('should select Restaurant in the dropdown menu', function(){
        restaurantClick.click();
    });
    it('should have a page title', function() {
        expect(pageTitle.getText()).toContain('Restaurant');
    });
    it('should have a field to type the name of the restaurant', function() {
        expect(restaurantDescriptionField.isPresent()).toBe(true);
    });   
    it('should have a button to submit the form', function() {
        expect(buttonSave.isPresent()).toBe(true);
    });     
});
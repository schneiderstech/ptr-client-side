adminApp.service('RestaurantServices', function ($http, $mdToast) {
    let message = {
        fetchRestaurants: 'Error fetching Restaurants'
    };

    this.getRestaurants = function(){
        let promise;
        let myService = {
            async: function() {
            if ( !promise ) {
                promise = $http({
                            method  : 'GET',
                            url     : '/get-all-restaurants'
                        }).then(
                            function successCallback(response){
                            return response.data;       
                        }, function errorCallback(response){
                            $mdToast.show(
                            $mdToast.simple()
                                .textContent(message.fetchRestaurants)
                                .hideDelay(3000)
                            );
                        });
            }
            return promise;
            }
        };
        return myService;
    };    
});
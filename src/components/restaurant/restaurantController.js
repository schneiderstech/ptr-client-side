(function() { 
    'use strict';

    adminApp.controller('RestaurantController', ['$scope', '$http', '$mdToast', 'RestaurantServices', function($scope, $http, $mdToast, RestaurantServices) {
        $scope.state = {
            restaurant: {},
            message:{
                required: 'Check required fields mate!',
                success: 'Restaurant added!'
            }            
        };

        $scope.icons = {
            path: '/assets/img/icons/', 
            icon: {
                restaurant: 'ic_local_dining_24px.svg'
            }
        } 

        let state = $scope.state;

        let refresh = function() {
            RestaurantServices.getRestaurants().async().then(function(data) {
                state.listRestaurants = data;
            });              
        };

        refresh();

        $scope.submitRestaurant = function(valid) {
            if($scope.formAddRestaurant.$valid) {
                $http({
                    method  : 'POST',
                    url     : '/add-new-restaurant',
                    data    : state.restaurant
                })
                .then(function successCallback(response){
                    $mdToast.show(
                    $mdToast.simple()
                        .textContent(state.message.success)
                        .hideDelay(3000)
                    );
                    refresh(); 
                }, function errorCallback(response){
                    $mdToast.show(
                    $mdToast.simple()
                        .textContent(response)
                        .hideDelay(3000)
                    );
                });
            } else {
                $mdToast.show(
                $mdToast.simple()
                    .textContent(state.message.required)
                    .hideDelay(3000)
                );
            }
        };
    }]);
})();
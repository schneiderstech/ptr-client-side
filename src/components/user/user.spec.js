describe('Angular Material User Section', function() {
    let menuClick = element(by.id('open-menu'));
    let userClick = element(by.id('userPage'));
    let pageTitle = element(by.name('page-title'));
    let userNameField = element(by.name('name'));
    let buttonSave = element(by.name('submitForm'));

    it('should click in the menu icon', function(){
        browser.get('http://localhost:8080')
        .then(function(){
            menuClick.click();
        });
    });
    it('should select User in the dropdown menu', function(){
        userClick.click();
    });
    it('should have a page title', function() {
        expect(pageTitle.getText()).toContain('User');
    });
    it('should have a field to type the name of the user', function() {
        expect(userNameField.isPresent()).toBe(true);
    });
    it('should have a button to submit the form', function() {
        expect(buttonSave.isPresent()).toBe(true);
    });        
});
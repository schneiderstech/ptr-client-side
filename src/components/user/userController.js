(function() { 
    'use strict';

    adminApp.controller('UserController', ['$scope', '$http', '$mdToast', 'UserServices', function($scope, $http, $mdToast, UserServices) {
        $scope.state = {
            user: {},
            message:{
                required: 'Check required fields mate!',
                success: 'User added!',
            }            
        };

        $scope.icons = {
            path: '/assets/img/icons/', 
            icon: {
                person: 'ic_person_24px.svg'
            }
        } 

        let state = $scope.state;

        let refresh = function() {
            UserServices.getUsers().async().then(function(data) {
                state.listUsers = data;
            });
        };

        refresh();

        $scope.submitUser = function(valid) {
            if($scope.formAddUser.$valid) {
                $http({
                    method  : 'POST',
                    url     : '/add-new-user',
                    data    : state.user
                })
                .then(function successCallback(response){
                    $mdToast.show(
                    $mdToast.simple()
                        .textContent(state.message.success)
                        .hideDelay(3000)
                    );
                    refresh(); 

                }, function errorCallback(response){
                    $mdToast.show(
                    $mdToast.simple()
                        .textContent(response)
                        .hideDelay(3000)
                    );                });
            } else {
                $mdToast.show(
                $mdToast.simple()
                    .textContent(state.message.required)
                    .hideDelay(3000)
                );
            }
        };
    }]);
})();
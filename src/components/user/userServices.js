adminApp.service('UserServices', function ($http, $mdToast) {
    let message = {
        fetchUsers: 'Error fetching Users'
    };

    this.getUsers = function(){
        let promise;
        let myService = {
            async: function() {
            if ( !promise ) {
                promise = $http({
                            method  : 'GET',
                            url     : '/get-all-users'
                        }).then(
                            function successCallback(response){
                            return response.data;       
                        }, function errorCallback(response){
                            $mdToast.show(
                            $mdToast.simple()
                                .textContent(message.fetchUsers)
                                .hideDelay(3000)
                            );
                        });
            }
            return promise;
            }
        };
        return myService;
    }; 
});
describe('Angular Material Main Section', function() {
    let pageTitle = element(by.name('page-title'));
    let userIdField = element(by.name('user'));
    let restaurantIdField = element(by.name('restaurant'));
    let dateIdField = element(by.name('date'));
    let buttonSave = element(by.name('submitForm'));
    
    it('should browser in the main page', function(){
        browser.get('http://localhost:8080');
    });
    
    it('should have a page title', function() {
        expect(pageTitle.getText()).toContain('Restaurant of the Week');
    });

    it('should have a select field for user', function() {
        expect(userIdField.isPresent()).toBe(true);
    });

    it('should have a select field for restaurant', function() {
        expect(restaurantIdField.isPresent()).toBe(true);
    });

    it('should have a select field for date', function() {
        expect(dateIdField.isPresent()).toBe(true);
    });        

    it('should have a button to submit the form', function() {
        expect(buttonSave.isPresent()).toBe(true);
    });     
});
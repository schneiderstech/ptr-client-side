adminApp.service('MainServices', function ($http, $mdToast) {
    let message = {
        fetchVotes: 'Error fetching Votes',
        fetchUsers: 'Error fetching Users',
        fetchRestaurants: 'Error fetching Restaurants'
    };

    this.getWeekDays = function () { 

        let currentDate = moment();
        let weekStart = currentDate.clone().startOf('week');
        let weekEnd = currentDate.clone().endOf('week');
        let days = [];

        for (var i = 0; i <= 6; i++) {
            let dayFormats = {};
            dayFormats.dateVoted = moment(weekStart).add(i, 'days').format();
            dayFormats.pretty = moment(weekStart).add(i, 'days').format("MMMM Do, dddd");
            days.push(dayFormats);
        };
        
        return days;
    };

    this.getUsers = function(){
        let promise;
        let myService = {
            async: function() {
            if ( !promise ) {
                // $http returns a promise, which has a then function, which also returns a promise
                promise = $http({
                            method  : 'GET',
                            url     : '/get-all-users'
                        }).then(
                            function successCallback(response){
                            return response.data;       
                        }, function errorCallback(response){
                            $mdToast.show(
                            $mdToast.simple()
                                .textContent(message.fetchUsers)
                                .hideDelay(3000)
                            );
                        });
            }
            // Return the promise to the controller
            return promise;
            }
        };
        return myService;
    };

    this.getRestaurants = function(){
        let promise;
        let myService = {
            async: function() {
            if ( !promise ) {
                promise = $http({
                            method  : 'GET',
                            url     : '/get-all-restaurants'
                        }).then(
                            function successCallback(response){
                            return response.data;       
                        }, function errorCallback(response){
                            $mdToast.show(
                            $mdToast.simple()
                                .textContent(message.fetchRestaurants)
                                .hideDelay(3000)
                            );
                        });
            }
            return promise;
            }
        };
        return myService;
    };

    this.getVotes = function(){
        let promise;
        let myService = {
            async: function() {
            if ( !promise ) {
                promise = $http({
                            method  : 'GET',
                            url     : '/get-all-votes-by-week'
                        }).then(
                            function successCallback(response){
                            return response.data;       
                        }, function errorCallback(response){
                            $mdToast.show(
                            $mdToast.simple()
                                .textContent(message.fetchVotes)
                                .hideDelay(3000)
                            );
                        });
            }
            return promise;
            }
        };
        return myService;
    };

    this.getTimeToVote = function(){
        let currentTime= moment();
        let startTime = moment('00:00 am', "HH:mm a");
        let endTime = moment('11:59 am', "HH:mm a");
        let amIBetween = currentTime.isBetween(startTime , endTime);

        return amIBetween;      
    };    
    
});
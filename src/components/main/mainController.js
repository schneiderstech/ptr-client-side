(function() { 
    'use strict';

    adminApp.controller('MainController', ['$scope', '$http', '$mdToast', 'MainServices', 'moment', function($scope, $http, $mdToast, MainServices, moment) {

        $scope.state = {
            listUsers: [],
            listRestaurants: [],
            listVotes: [],
            vote: {},
            weekDays: null,
            existInDay: 'existInDay',
            existInWeek: 'existInWeek',
            message:{
                existInDay: 'You have voted for that day :)',
                existInWeek: 'You have voted for this restaurant this week ;)',
                required: 'Check required fields mate!',
                success: 'Whoaa, let\'s go there...',
                noon: 'Votes are computed before noon, Sorry :|'             
            }
        };        

        let state = $scope.state;

        MainServices.getUsers().async().then(function(data) {
            state.listUsers = data;
        });

        MainServices.getRestaurants().async().then(function(data) {
            state.listRestaurants = data;
        });
        
        let refresh = function() {
            MainServices.getVotes().async().then(function(data) {
                state.listVotes = data;
            });
        };

        refresh();

        $scope.submitVote = function(valid) {
            if($scope.formAddVote.$valid) {

                let myVote = MainServices.getTimeToVote();

                if (myVote === true) {
                    $scope.makeItCount();
                } else {
                    $mdToast.show(
                    $mdToast.simple()
                        .textContent(state.message.noon)
                        .hideDelay(3000)
                    );
                }
            } else {
                $mdToast.show(
                $mdToast.simple()
                    .textContent(state.message.required)
                    .hideDelay(3000)
                );
            }
        };

        $scope.makeItCount = function(){
            $http({
                method  : 'POST',
                url     : '/add-new-vote',
                data    : state.vote
            })
            .then(function successCallback(response){
                if(response.data == state.existInDay){
                    $mdToast.show(
                    $mdToast.simple()
                        .textContent(state.message.existInDay)
                        .hideDelay(3000)
                    );                        
                }else if(response.data == state.existInWeek){
                    $mdToast.show(
                    $mdToast.simple()
                        .textContent(state.message.existInWeek)
                        .hideDelay(3000)
                    );
                }else{
                    $mdToast.show(
                    $mdToast.simple()
                        .textContent(state.message.success)
                        .hideDelay(3000)
                    );                        
                    refresh(); 
                }
            }, function errorCallback(response){
                $mdToast.show(
                $mdToast.simple()
                    .textContent(response)
                    .hideDelay(3000)
                );                        
            });
            
        };        

        state.weekDays = MainServices.getWeekDays(); 

    }]);
})();